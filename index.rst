.. |label| replace:: 1stVision Modul Kategoriegruppen mit Mehrfachselektion in Sage 100
.. |snippet| replace:: 1stVision.Kategorien
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 8.0
.. |maxVersion| replace:: 9.0
.. |version| replace:: 9.0.0.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:Kürzel: |snippet|
:kompatibel für Sage OfficeLine: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Die AppDesigner-Lösung |label| ist eine Art benutzerdefinierte Felder mit Mehrfachselektion. 
Es ist möglich beliebig viele Gruppen anzulegen und Elemente zu definieren die man selektieren kann, somit können einem Artikel in einer Gruppe auch mehrere Elemente zugeordnet werden.
Da im Standard auf USER-Felder bzw. in Sachmerkmalen jeweils nur ein Wert je Feld oder SML-Merkmal zugeordnet wird, kann man mit diesem Modul nun mehrere Werte hinterlegen.


Installation
------------
Es werden spezielle SQL-Scripte benötigt, die man ausführen muss.
Nachdem die Scripte ausgeführt wurden, kann die META-Datei im AppDesigner importiert werden.
Die Datei „1stVision.Kategorien.dll“ wird ins Shared-Verzeichnis durch das Installieren der App-Designer-Lösung kopiert.
Dann muss der Applikationsserver-Dienst neu gestartet werden.
Nun ist das Modul in der Sage 100 zum starten bereit.


Konfiguration / Einstellungen
-----------------------------
Im Hauptmenü der Sage 100 finden Sie nun den Punkt Kategorien. Hier können Sie Gruppen oder Elemente anlegen, die Sie für die Artikel definieren.

.. image:: multikat_1.jpg

Gruppen
_______
.. image:: multikat_2.jpg

Hier können Sie neue Gruppen anlegen, bearbeiten oder löschen. Jede Gruppe hat einen internen Gruppennamen und eine Bezeichnung.


Elemente
________

.. image:: multikat_3.jpg

Hier können Sie neue Elemente anlegen, bearbeiten oder löschen. Jedes Element hat einen internen Elementnamen und eine Bezeichnung.
Für ein neues Element wählen Sie die Gruppe aus dem dieses Element zugewiesen ist.

Artikelzuordnung
----------------

.. image:: multikat_4.jpg

In der Artikelübersichtsmaske finden Sie im Burger-Menü einen Eintrag "Kategorien".
Sie können diesen Eintrag auch als Schnell-Button hinterlegen.


.. image:: multikat_5.jpg

Nachdem Sie nun einen Artikel und dann den Button „Kategorien“ angeklickt haben, öffnet sich ein neues Fenster. Im oberen Bereich sehen Sie die Gruppen die Sie bereits angelegt haben. 
Wenn Sie eine Gruppe durch Anklicken markiert haben, werden im unteren Bereich die Elemente angezeigt, die Sie vorher der Gruppe zugeordnet haben. 
Nun können Sie durch Anhaken der Elemente und anschließendem Speichern die Elemente in den Gruppen den Artikeln zuweisen. 
Wenn Sie bereits eine Zuordnung hinterlegt haben, werden beim erneuten Öffnen dieser die Haken bei den Elementen gesetzt sein.
